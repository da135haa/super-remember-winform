﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Devices;


namespace 記憶大考驗
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int time = 0;
        Random rd = new Random();
        PictureBox[] px = new PictureBox[9];
        int[] a = new int[9];
        Computer myComputer = new Computer();
        int Difficulty = 0;
       
        private void timer1_Tick(object sender, EventArgs e)
        {
            time--;
            labelTime.Text = "您還有:" + time + "秒";
            
            if (time == 0)
            {
                timer1.Enabled = false;
                timer2.Enabled = true;
                time = 31;
                for (int i = 1; i < px.Length; i++)
                {
                    px[i].Enabled = true;
                    px[i].BackgroundImage = new Bitmap("0.jpg");
                }
            }
            
        }

        private void myButton(object sender, EventArgs e)
        {

            if (sender == button1)
            {
                time = 2;
                Difficulty = 1;
            }
            if (sender == button2) 
            {
                time = 5;
                Difficulty = 2;
            }
            if (sender == button3) 
            {
                time = 10;
                Difficulty = 3;
            }
            timer1.Enabled = true;
            timer2.Enabled = false;
            px[1] = pictureBox1;
            px[2] = pictureBox2;
            px[3] = pictureBox3;
            px[4] = pictureBox4;
            px[5] = pictureBox5;
            px[6] = pictureBox6;
            px[7] = pictureBox7;
            px[8] = pictureBox8;

            a[1] = rd.Next(1, 5);
            px[1].BackgroundImage = new Bitmap(a[1] + ".jpg");
           
            for (int i = 2; i < px.Length; i++)
            {
                a[i] = rd.Next(1, 5);
                int test = 0;
                for (int i1 = 1; i1 < i; i1++)
                {
                    
                    if (a[i1] == a[i])
                    {
                        test++;
                        if (test == 2)
                        {
                            i--;
                            break;
                        }                      
                    }
                    if (i1 == i - 1)
                    {                       
                        px[i].BackgroundImage = new Bitmap(a[i] + ".jpg");
                    }                                    
                }
                    
                px[i].Enabled = false;
            }
            
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            time--;
            labelTime.Text = "遊戲時間:" + time + "秒";
            

            
            if (time == 0)
            {
                timer2.Enabled = false;
                labelTime.Text = "請選擇級別來進行遊戲";
                MessageBox.Show("時間到,闖關失敗");
                
            }
        }
        int touch = 0;
        int one, two;
        private void Touch(object sender, EventArgs e)
        {
            PictureBox pxHit = (PictureBox)sender;
            pxHit.Enabled = false;
            touch++;
            if (touch == 1)
            {
                one = Array.IndexOf(px, pxHit);
                px[one].BackgroundImage = new Bitmap(a[one] + ".jpg");                              
            }
            else if (touch == 2)
            {
                two = Array.IndexOf(px, pxHit);
                px[two].BackgroundImage = new Bitmap(a[two] + ".jpg");

                if (a[one] == a[two])
                {
                    myComputer.Audio.Play("CHIMES.WAV", AudioPlayMode.Background);
                    touch = 0;                   
                }
                else
                {
                    MessageBox.Show("答錯了^_|||");
                    px[one].BackgroundImage = new Bitmap("0.jpg");
                    px[two].BackgroundImage = new Bitmap("0.jpg");
                    touch = 0;                   
                    px[one].Enabled = true;
                    px[two].Enabled = true;
                }  
            }
           
            for (int i = 1; i <= px.Length-1; i++)
            {
                if (px[i].Enabled == true)
                {
                    break;
                }
                if (i == px.Length-1)
                {
                    timer2.Enabled = false;
                    if (Difficulty == 1)
                    {
                        MessageBox.Show("通關...果然是記憶高手");
                    }
                    if (Difficulty == 2)
                    {
                        MessageBox.Show("通關...你的記憶力還不錯");
                    }
                    if (Difficulty == 3)
                    {
                        MessageBox.Show("通關...你的記憶力馬馬乎乎");
                    }
                    myComputer.Audio.Play("APPLAUSE.WAV", AudioPlayMode.Background);

                }
            }
            
        }
    }
}
